use crate::{Coordinates, Error};

use crate::network;
use crate::network::Protocol;

use futures::future::join_all;

use tokio::net::TcpStream;

use serde::{Deserialize, Serialize};

/// Valid plays
#[derive(Copy, Clone, Debug, Deserialize, PartialEq, Serialize)]
pub enum Play {
    X,
    O,
    Empty,
}

/// Contains the plays
#[derive(Debug, Copy, Clone)]
struct MiniBoard {
    values: [[Play; 3]; 3],
    win: Option<Play>,
}

impl MiniBoard {
    /// Create a new instance with all values Empty
    fn new() -> Self {
        Self {
            values: [[Play::Empty; 3]; 3],
            win: None,
        }
    }

    /// Play into cell
    fn play(&mut self, coords: Coordinates, value: Play) -> Result<(), Error> {
        if let Some(_) = self.win {
            return Err(Error::InvalidPlay);
        }

        let cell: &mut Play = &mut self.values[coords.x() as usize][coords.y() as usize];

        match *cell {
            Play::Empty => {
                *cell = value;
                self.check();
                Ok(())
            }
            _ => Err(Error::InvalidPlay),
        }
    }

    /// Check if win
    fn win(&self) -> bool {
        self.win != None && self.win != Some(Play::Empty)
    }

    /// Get winner
    fn winner(&self) -> Option<Play> {
        self.win
    }

    /// Check if there is a win or draw
    fn check(&mut self) {
        if self.win != None {
            return;
        }

        self.win = Some(Play::Empty);

        // Check both diagonals
        if (self.values[0][0] != Play::Empty
            && self.values[0][0] == self.values[1][1]
            && self.values[1][1] == self.values[2][2])
            || (self.values[0][2] != Play::Empty
                && self.values[0][2] == self.values[1][1]
                && self.values[1][1] == self.values[2][0])
        {
            self.win = Some(self.values[1][1]);
        } else {
            for i in 0..3 {
                // Check horizontal win
                if self.values[i][0] != Play::Empty
                    && self.values[i][0] == self.values[i][1]
                    && self.values[i][1] == self.values[i][2]
                {
                    self.win = Some(self.values[i][0]);

                    return;
                // Check vertical win
                } else if self.values[0][i] != Play::Empty
                    && self.values[0][i] == self.values[1][i]
                    && self.values[1][i] == self.values[2][i]
                {
                    self.win = Some(self.values[0][i]);

                    return;
                // Check for empty cells
                } else if self.values[0][i] == Play::Empty
                    || self.values[1][i] == Play::Empty
                    || self.values[2][i] == Play::Empty
                {
                    self.win = None;
                }
            }
        }
    }
}

/// Contains the nine "mini" boards
#[derive(Debug)]
struct SuperBoard {
    boards: [[MiniBoard; 3]; 3],
}

impl SuperBoard {
    /// Create a new instance with all empty MiniBoards
    fn new() -> Self {
        Self {
            boards: [[MiniBoard::new(); 3]; 3],
        }
    }

    /// Play on the given MiniBoard
    pub fn play(
        &mut self,
        super_coords: Coordinates,
        mini_coords: Coordinates,
        play: Play,
    ) -> Result<(), Error> {
        self.boards[super_coords.x() as usize][super_coords.y() as usize].play(mini_coords, play)
    }

    /// Select a Miniboard instance
    fn get(&mut self, index: Coordinates) -> &mut MiniBoard {
        &mut self.boards[index.x() as usize][index.y() as usize]
    }

    /// Check if there is a win or draw
    fn check(&mut self) -> Option<Play> {
        let mut winner: Option<Play> = Some(Play::Empty);

        // Check both diagonals
        if (self.boards[0][0].win()
            && self.boards[0][0].winner() == self.boards[1][1].winner()
            && self.boards[1][1].winner() == self.boards[2][2].winner())
            || (self.boards[0][2].win()
                && self.boards[0][2].winner() == self.boards[1][1].winner()
                && self.boards[1][1].winner() == self.boards[2][0].winner())
        {
            return self.boards[1][1].winner();
        } else {
            for i in 0..3 {
                // Check horizontal win
                if self.boards[i][0].win()
                    && self.boards[i][0].winner() == self.boards[i][1].winner()
                    && self.boards[i][1].winner() == self.boards[i][2].winner()
                {
                    return self.boards[i][0].winner();
                // Check vertical win
                } else if self.boards[0][i].win()
                    && self.boards[0][i].winner() == self.boards[1][i].winner()
                    && self.boards[1][i].winner() == self.boards[2][i].winner()
                {
                    return self.boards[0][i].winner();
                // Check for empty cells
                } else if (self.boards[0][i].winner() == None)
                    || (self.boards[1][i].winner() == None)
                    || (self.boards[2][i].winner() == None)
                {
                    winner = None;
                }
            }
        }

        return winner;
    }
}

/// Number of players in the game
#[derive(Debug)]
enum GameStatus {
    Empty,
    Half(Option<TcpStream>),
    Full(Option<TcpStream>, Option<TcpStream>),
}

impl GameStatus {
    fn upgrade(&mut self, new: TcpStream) {
        *self = match *self {
            GameStatus::Empty => GameStatus::Half(Some(new)),
            GameStatus::Half(ref mut old) => {
                // First element of the tuple plays with O
                if rand::random() {
                    GameStatus::Full(old.take(), Some(new))
                } else {
                    GameStatus::Full(Some(new), old.take())
                }
            }
            GameStatus::Full(_, _) => unreachable!(),
        }
    }

    fn get_streams(&mut self) -> (TcpStream, TcpStream) {
        match self {
            GameStatus::Full(ref mut s1, ref mut s2) => (s1.take().unwrap(), s2.take().unwrap()),
            _ => unreachable!(),
        }
    }
}

/// Contains an instance of a game
#[derive(Debug)]
pub struct Game {
    board: SuperBoard,
    current_super: Option<Coordinates>,
    current_play: Play,
    players: GameStatus,
}

impl Game {
    /// Create a new instance of a game with a random Uuid
    pub fn new() -> Self {
        Self {
            board: SuperBoard::new(),
            current_super: Option::None,
            current_play: Play::O,
            players: GameStatus::Empty,
        }
    }

    /// Get number of players
    pub fn len(&self) -> u8 {
        match self.players {
            GameStatus::Empty => 0,
            GameStatus::Half(_) => 1,
            GameStatus::Full(_, _) => 2,
        }
    }

    /// Add player to game
    pub fn add(&mut self, player: TcpStream) {
        self.players.upgrade(player);
    }

    /// Start the game
    pub async fn start(&mut self) -> Result<(), Error> {
        let (mut player1, mut player2) = self.players.get_streams();

        let w_send_play = vec![
            network::set_play(&mut player1, Play::O),
            network::set_play(&mut player2, Play::X),
        ];

        for (i, result) in join_all(w_send_play).await.iter().enumerate() {
            match result {
                Ok(_) => (),
                Err(_) => {
                    if i == 0 {
                        return network::handle_end(&mut player2).await;
                    } else {
                        return network::handle_end(&mut player1).await;
                    }
                }
            }
        }

        let mut end_status: Option<Play> = None;
        while end_status == None {
            match self.current_play {
                Play::O => match network::get_play(&mut player1).await {
                    Ok(m) => {
                        if let Protocol::Play {
                            super_coords: s_coords,
                            mini_coords: m_coords,
                        } = m
                        {
                            match self.play(s_coords, m_coords) {
                                Ok(()) => {
                                    match network::ok(&mut player1).await {
                                        Ok(_) => (),
                                        Err(_) => return network::handle_end(&mut player2).await,
                                    };
                                    match network::send_status(&mut player2, s_coords, m_coords)
                                        .await
                                    {
                                        Ok(_) => (),
                                        Err(_) => return network::handle_end(&mut player1).await,
                                    };
                                }
                                Err(_) => match network::nok(&mut player1).await {
                                    Ok(_) => (),
                                    Err(_) => return network::handle_end(&mut player2).await,
                                },
                            };
                        } else {
                            unreachable!();
                        }
                    }
                    Err(_) => panic!("Couldn't get play"),
                },
                Play::X => match network::get_play(&mut player2).await {
                    Ok(m) => {
                        if let Protocol::Play {
                            super_coords: s_coords,
                            mini_coords: m_coords,
                        } = m
                        {
                            match self.play(s_coords, m_coords) {
                                Ok(()) => {
                                    match network::ok(&mut player2).await {
                                        Ok(_) => (),
                                        Err(_) => return network::handle_end(&mut player1).await,
                                    };
                                    match network::send_status(&mut player1, s_coords, m_coords)
                                        .await
                                    {
                                        Ok(_) => (),
                                        Err(_) => return network::handle_end(&mut player2).await,
                                    };
                                }
                                Err(_) => match network::nok(&mut player2).await {
                                    Ok(_) => (),
                                    Err(_) => return network::handle_end(&mut player1).await,
                                },
                            };
                        } else {
                            unreachable!();
                        }
                    }
                    Err(_) => panic!("Couldn't get play"),
                },
                _ => unreachable!(),
            };

            end_status = self.board.check();
        }

        Ok(())
    }

    /// Play in the given superboard and miniboard
    fn play(&mut self, superb: Coordinates, minib: Coordinates) -> Result<(), Error> {
        if let Some(_) = self.current_super {
            if self.current_super.unwrap() != superb {
                return Err(Error::InvalidPlay);
            }
        }

        match self.board.play(superb, minib, self.current_play) {
            Ok(_) => (),
            Err(e) => return Err(e),
        }

        if self.board.get(minib).winner() == None {
            self.current_super = Some(minib);
        } else {
            self.current_super = None;
        }

        self.current_play = match self.current_play {
            Play::O => Play::X,
            Play::X => Play::O,
            _ => unreachable!(),
        };

        Ok(())
    }
}

// Testing
#[cfg(test)]
mod game_tests {
    use crate::game::*;

    fn assert_miniboard(board: &MiniBoard, index: Coordinates, value: Play) {
        for i in 0..3 {
            for j in 0..3 {
                if i == index.x() && j == index.y() {
                    assert_eq!(board.values[index.0 as usize][index.1 as usize], value);
                } else {
                    assert_eq!(board.values[i as usize][j as usize], Play::Empty);
                }
            }
        }
    }

    fn assert_empty_miniboard(board: &MiniBoard) {
        for i in 0..3 {
            for j in 0..3 {
                assert_eq!(board.values[i as usize][j as usize], Play::Empty);
            }
        }
    }

    #[test]
    fn play_x_on_miniboard() {
        let mut board = MiniBoard::new();
        let coords = Coordinates::new(2, 1).unwrap();

        assert_eq!(board.play(coords, Play::X), Ok(()));

        assert_miniboard(&board, coords, Play::X);
    }

    #[test]
    fn play_o_on_miniboard_through_superboard() {
        let mut board = SuperBoard::new();

        let coords = Coordinates::new(1, 1).unwrap();
        let play: &mut MiniBoard = board.get(coords);

        assert_eq!(play.play(coords, Play::O), Ok(()));

        assert_miniboard(&play, coords, Play::O);

        assert_empty_miniboard(board.get(Coordinates::new(0, 0).unwrap()));
        assert_empty_miniboard(board.get(Coordinates::new(0, 1).unwrap()));
        assert_empty_miniboard(board.get(Coordinates::new(0, 2).unwrap()));
        assert_empty_miniboard(board.get(Coordinates::new(1, 0).unwrap()));
        assert_empty_miniboard(board.get(Coordinates::new(1, 2).unwrap()));
        assert_empty_miniboard(board.get(Coordinates::new(2, 0).unwrap()));
        assert_empty_miniboard(board.get(Coordinates::new(2, 1).unwrap()));
        assert_empty_miniboard(board.get(Coordinates::new(2, 2).unwrap()));
    }

    #[test]
    fn check_current_super_after_play() {
        let mut board = Game::new();

        assert_eq!(board.current_super, Option::None);

        assert_eq!(
            board.play(
                Coordinates::new(1, 2).unwrap(),
                Coordinates::new(0, 2).unwrap(),
            ),
            Ok(())
        );

        assert_eq!(board.current_super, Some(Coordinates::new(0, 2).unwrap()));
    }

    #[test]
    fn check_current_super_with_superboard_full() {
        let mut board = Game::new();

        board.board.boards[0][0] = MiniBoard {
            values: [[Play::O; 3]; 3],
            win: Some(Play::O),
        };

        assert_eq!(board.current_super, Option::None);

        assert_eq!(
            board.play(
                Coordinates::new(2, 2).unwrap(),
                Coordinates::new(0, 0).unwrap(),
            ),
            Ok(())
        );

        assert_eq!(board.current_super, Option::None);
    }

    // MiniBoard win tests

    #[test]
    fn check_miniboard_winner_diagonal() {
        let mut board = MiniBoard {
            values: [
                [Play::X, Play::Empty, Play::Empty],
                [Play::Empty, Play::X, Play::Empty],
                [Play::Empty, Play::Empty, Play::X],
            ],
            win: None,
        };

        board.check();
        assert_eq!(board.win(), true);
        assert_eq!(board.winner(), Some(Play::X));

        board = MiniBoard {
            values: [
                [Play::Empty, Play::Empty, Play::O],
                [Play::Empty, Play::O, Play::Empty],
                [Play::O, Play::Empty, Play::Empty],
            ],
            win: None,
        };

        board.check();
        assert_eq!(board.win(), true);
        assert_eq!(board.winner(), Some(Play::O));
    }

    #[test]
    fn check_miniboard_winner_vertical() {
        let mut board = MiniBoard {
            values: [
                [Play::O, Play::Empty, Play::Empty],
                [Play::O, Play::Empty, Play::Empty],
                [Play::O, Play::Empty, Play::Empty],
            ],
            win: None,
        };

        board.check();
        assert_eq!(board.win(), true);
        assert_eq!(board.winner(), Some(Play::O));

        board = MiniBoard {
            values: [
                [Play::Empty, Play::O, Play::Empty],
                [Play::Empty, Play::O, Play::Empty],
                [Play::Empty, Play::O, Play::Empty],
            ],
            win: None,
        };

        board.check();
        assert_eq!(board.win(), true);
        assert_eq!(board.winner(), Some(Play::O));

        board = MiniBoard {
            values: [
                [Play::Empty, Play::Empty, Play::O],
                [Play::Empty, Play::Empty, Play::O],
                [Play::Empty, Play::Empty, Play::O],
            ],
            win: None,
        };

        board.check();
        assert_eq!(board.win(), true);
        assert_eq!(board.winner(), Some(Play::O));
    }

    #[test]
    fn check_miniboard_winner_horizontal() {
        let mut board = MiniBoard {
            values: [
                [Play::X, Play::X, Play::X],
                [Play::Empty, Play::Empty, Play::Empty],
                [Play::Empty, Play::Empty, Play::Empty],
            ],
            win: None,
        };

        board.check();
        assert_eq!(board.win(), true);
        assert_eq!(board.winner(), Some(Play::X));

        board = MiniBoard {
            values: [
                [Play::Empty, Play::Empty, Play::Empty],
                [Play::X, Play::X, Play::X],
                [Play::Empty, Play::Empty, Play::Empty],
            ],
            win: None,
        };

        board.check();
        assert_eq!(board.win(), true);
        assert_eq!(board.winner(), Some(Play::X));

        board = MiniBoard {
            values: [
                [Play::Empty, Play::Empty, Play::Empty],
                [Play::Empty, Play::Empty, Play::Empty],
                [Play::X, Play::X, Play::X],
            ],
            win: None,
        };

        board.check();
        assert_eq!(board.win(), true);
        assert_eq!(board.winner(), Some(Play::X));
    }

    #[test]
    fn check_miniboard_draw() {
        let mut board = MiniBoard {
            values: [
                [Play::X, Play::O, Play::X],
                [Play::X, Play::O, Play::X],
                [Play::O, Play::X, Play::O],
            ],
            win: None,
        };

        board.check();
        assert_eq!(board.win(), false);
        assert_eq!(board.winner(), Some(Play::Empty));
    }

    #[test]
    fn check_miniboard_not_end() {
        let mut board = MiniBoard {
            values: [
                [Play::X, Play::O, Play::Empty],
                [Play::X, Play::O, Play::X],
                [Play::O, Play::X, Play::O],
            ],
            win: None,
        };

        board.check();
        assert_eq!(board.win(), false);
        assert_eq!(board.winner(), None);
    }

    // SuperBoard win tests

    #[test]
    fn check_superboard_winner_diagonal() {
        let mut mini = MiniBoard::new();
        mini.win = Some(Play::X);
        let empty = MiniBoard::new();

        let mut board = SuperBoard {
            boards: [
                [mini, empty, empty],
                [empty, mini, empty],
                [empty, empty, mini],
            ],
        };

        assert_eq!(board.check(), Some(Play::X));

        board = SuperBoard {
            boards: [
                [empty, empty, mini],
                [empty, mini, empty],
                [mini, empty, empty],
            ],
        };

        assert_eq!(board.check(), Some(Play::X));
    }

    #[test]
    fn check_superboard_winner_vertical() {
        let mut mini = MiniBoard::new();
        mini.win = Some(Play::O);
        let empty = MiniBoard::new();

        let mut board = SuperBoard {
            boards: [
                [mini, empty, empty],
                [mini, empty, empty],
                [mini, empty, empty],
            ],
        };

        assert_eq!(board.check(), Some(Play::O));

        board = SuperBoard {
            boards: [
                [empty, mini, empty],
                [empty, mini, empty],
                [empty, mini, empty],
            ],
        };

        assert_eq!(board.check(), Some(Play::O));

        board = SuperBoard {
            boards: [
                [empty, empty, mini],
                [empty, empty, mini],
                [empty, empty, mini],
            ],
        };

        assert_eq!(board.check(), Some(Play::O));
    }

    #[test]
    fn check_superboard_winner_horizontal() {
        let mut mini = MiniBoard::new();
        mini.win = Some(Play::X);
        let empty = MiniBoard::new();

        let mut board = SuperBoard {
            boards: [
                [mini, mini, mini],
                [empty, empty, empty],
                [empty, empty, empty],
            ],
        };

        assert_eq!(board.check(), Some(Play::X));

        board = SuperBoard {
            boards: [
                [empty, empty, empty],
                [mini, mini, mini],
                [empty, empty, empty],
            ],
        };

        assert_eq!(board.check(), Some(Play::X));

        board = SuperBoard {
            boards: [
                [empty, empty, empty],
                [empty, empty, empty],
                [mini, mini, mini],
            ],
        };

        assert_eq!(board.check(), Some(Play::X));
    }

    #[test]
    fn check_superboard_draw() {
        let mut mini_o = MiniBoard::new();
        mini_o.win = Some(Play::O);
        let mut mini_x = MiniBoard::new();
        mini_x.win = Some(Play::X);

        let mut board = SuperBoard {
            boards: [
                [mini_x, mini_o, mini_x],
                [mini_x, mini_o, mini_x],
                [mini_o, mini_x, mini_o],
            ],
        };

        assert_eq!(board.check(), Some(Play::Empty));
    }

    #[test]
    fn check_superboard_not_end() {
        let mut mini_o = MiniBoard::new();
        mini_o.win = Some(Play::O);
        let mut mini_x = MiniBoard::new();
        mini_x.win = Some(Play::X);
        let empty = MiniBoard::new();

        let mut board = SuperBoard {
            boards: [
                [mini_x, mini_o, empty],
                [mini_x, mini_o, mini_x],
                [mini_o, mini_x, mini_o],
            ],
        };

        assert_eq!(board.check(), None);
    }
}
