use std::collections::{HashMap, HashSet};

use supergalo_server::game::Game;
use supergalo_server::network;
use supergalo_server::network::Protocol;

use lazy_static::lazy_static;

use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::{TcpListener, TcpStream};
use tokio::sync::Mutex;

use uuid::Uuid;

const BIND_ADDR: &str = "0.0.0.0:3000";

lazy_static! {
    static ref GAMES: Mutex<HashMap<Uuid, Game>> = Mutex::new(HashMap::new());
    static ref AVAILABLE_GAMES: Mutex<HashSet<Uuid>> = Mutex::new(HashSet::new());
}

async fn accept(
    all_games: &Mutex<HashMap<Uuid, Game>>,
    free_games: &Mutex<HashSet<Uuid>>,
    mut stream: TcpStream,
) -> std::io::Result<()> {
    let message = match network::get_join(&mut stream).await {
        Ok(m) => m,
        Err(_) => {
            println!("[ERROR] Invalid message");
            stream.shutdown().await?;
            return Ok(());
        }
    };

    let game_id = match message {
        /* Join any available game */
        Protocol::Join { game_id: None } => {
            let mut free_guard = free_games.lock().await;
            let id: Uuid;

            // Check for available games
            if free_guard.is_empty() {
                let game = Game::new();

                id = Uuid::new_v4();

                let mut all_guard = all_games.lock().await;
                all_guard.insert(id, game);
            } else {
                id = *free_guard
                    .iter()
                    .map(|e| *e)
                    .collect::<Vec<Uuid>>()
                    .get(0)
                    .unwrap();

                free_guard.take(&id);
            }

            id
        }
        /* Join to a game with given Uuid */
        Protocol::Join {
            game_id: Some(game),
        } => {
            let mut free_guard = free_games.lock().await;

            /* Check if game is already created and is free */
            if let Some(_) = free_guard.take(&game) {
                game
            } else {
                let mut all_guard = all_games.lock().await;

                /* Check if game is created but full */
                if all_guard.contains_key(&game) {
                    match network::nok(&mut stream).await {
                        Ok(_) => (),
                        Err(_) => println!("[ERROR] Couldn't send NOK message"),
                    };

                    return Ok(());
                } else {
                    all_guard.insert(game, Game::new());
                    game
                }
            }
        }
        _ => {
            println!("[ERROR] Invalid JOIN message");

            return Ok(());
        }
    };

    match network::joined(&mut stream, game_id).await {
        Ok(_) => (),
        Err(_) => println!("[ERROR] Couldn't send JOINED message"),
    };

    let mut game: Option<Game> = None;
    let mut guard = all_games.lock().await;

    let chosen = guard.get_mut(&game_id).unwrap();

    // Check if game can be started or not
    chosen.add(stream);
    if chosen.len() == 2 {
        game = Some(guard.remove(&game_id).unwrap());
    } else {
        let mut guard = free_games.lock().await;
        guard.insert(game_id);
    }

    // Release the lock
    std::mem::drop(guard);

    match game {
        Some(mut game) => match game.start().await {
            Ok(_) => println!("Match went OK"),
            Err(e) => println!("Match with error {:#?}", e),
        },
        _ => (),
    };

    Ok(())
}

#[tokio::main]
async fn main() -> std::io::Result<()> {
    let listener = TcpListener::bind(BIND_ADDR).await?;

    println!("Server started in {}", BIND_ADDR);

    loop {
        let (stream, _) = listener.accept().await?;
        println!("[NEW] Connection from {}", stream.peer_addr().unwrap());
        tokio::task::spawn(accept(&GAMES, &AVAILABLE_GAMES, stream));
    }
}
