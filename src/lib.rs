pub mod game;
pub mod network;

use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq)]
pub enum Error {
    BothPlayersDown,   // Both players lost connection
    InvalidCoordValue, // Wrong coordinate value
    InvalidMessage,    // A message could not be deserialized or is incorrect in the current context
    InvalidPlay,
    IOError,
    OnePlayerDown, // One of the players lost connection
}

#[derive(Clone, Copy, Debug, Deserialize, PartialEq, Serialize)]
/// Coordinates of the 3x3 grids used in the game
pub struct Coordinates(u8, u8);

impl Coordinates {
    /// Create new instance of Coordinates with the appropriate restrictions
    pub fn new(x: u8, y: u8) -> Result<Self, Error> {
        if x > 2 || y > 2 {
            return Err(Error::InvalidCoordValue);
        }

        Ok(Self(x, y))
    }

    /// Check if restrictions meet
    pub fn ok(&self) -> bool {
        self.0 <= 2 && self.1 <= 2
    }

    /// Get the first coordinate
    pub fn x(&self) -> u8 {
        self.0
    }

    /// Get the second coordinate
    pub fn y(&self) -> u8 {
        self.1
    }
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn create_valid_coordinates() {
        let coords = Coordinates::new(1, 2).unwrap();

        assert_eq!(coords.x(), 1);
        assert_eq!(coords.y(), 2);
    }

    #[test]
    fn create_invalid_coordinates() {
        let mut coords = Coordinates::new(1, 3);
        match coords {
            Err(error) if error == Error::InvalidCoordValue => {}
            _ => panic!("Error::InvalidCoordValue should be thrown"),
        }

        coords = Coordinates::new(3, 1);
        match coords {
            Err(error) if error == Error::InvalidCoordValue => {}
            _ => panic!("Error::InvalidCoordValue should be thrown"),
        }
    }
}
