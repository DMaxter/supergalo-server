use crate::game::Play;
use crate::{Coordinates, Error};

use bincode;

use serde::{Deserialize, Serialize};

use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::TcpStream;

use uuid::Uuid;

const MAX_READ_BYTES: usize = 1024;

#[derive(PartialEq, Deserialize, Serialize)]
pub enum Protocol {
    Joined {
        game_id: Uuid,
    },
    Ok,
    Nok,
    SetPlay {
        board_play: Play,
    },
    Status {
        super_coords: Coordinates,
        mini_coords: Coordinates,
    },
    Winner {
        player_id: Play,
    },
    Draw,
    EndGame,
    Join {
        game_id: Option<Uuid>,
    },
    Play {
        super_coords: Coordinates,
        mini_coords: Coordinates,
    },
}

/// Serialize and send a message through the socket
async fn send(stream: &mut TcpStream, message: &Protocol) -> std::io::Result<()> {
    stream
        .write_all(&bincode::serialize(&message).unwrap())
        .await?;

    stream.flush().await?;

    Ok(())
}

/// Send information to player trying to join
pub async fn joined(stream: &mut TcpStream, game: Uuid) -> Result<(), Error> {
    match send(stream, &Protocol::Joined { game_id: game }).await {
        Ok(_) => Ok(()),
        Err(_) => Err(Error::IOError),
    }
}

/// Send information to acknowledge the operation
pub async fn ok(stream: &mut TcpStream) -> Result<(), Error> {
    match send(stream, &Protocol::Ok).await {
        Ok(_) => Ok(()),
        Err(_) => Err(Error::IOError),
    }
}

/// Send information stating that the operation is invalid
pub async fn nok(stream: &mut TcpStream) -> Result<(), Error> {
    match send(stream, &Protocol::Nok).await {
        Ok(_) => Ok(()),
        Err(_) => Err(Error::IOError),
    }
}

/// Send the playing symbol to the player
pub async fn set_play(stream: &mut TcpStream, play: Play) -> Result<(), Error> {
    match send(stream, &Protocol::SetPlay { board_play: play }).await {
        Ok(_) => Ok(()),
        Err(_) => Err(Error::IOError),
    }
}

/// Send the other player's play
pub async fn send_status(
    stream: &mut TcpStream,
    coords_super: Coordinates,
    coords_mini: Coordinates,
) -> Result<(), Error> {
    match send(
        stream,
        &Protocol::Status {
            super_coords: coords_super,
            mini_coords: coords_mini,
        },
    )
    .await
    {
        Ok(_) => Ok(()),
        Err(_) => Err(Error::IOError),
    }
}

/// Send game winner message
pub async fn announce_winner(stream: &mut TcpStream, winner: Play) -> Result<(), Error> {
    match send(stream, &Protocol::Winner { player_id: winner }).await {
        Ok(_) => Ok(()),
        Err(_) => Err(Error::IOError),
    }
}

/// Send draw message
pub async fn announce_draw(stream: &mut TcpStream) -> Result<(), Error> {
    match send(stream, &Protocol::Draw).await {
        Ok(_) => Ok(()),
        Err(_) => Err(Error::IOError),
    }
}

/// End game without a winner/draw
///
/// Best suited for unrecoverable errors
pub async fn end_game(stream: &mut TcpStream) -> Result<(), Error> {
    match send(stream, &Protocol::EndGame).await {
        Ok(_) => Ok(()),
        Err(_) => Err(Error::IOError),
    }
}

/// Get a message from the socket and deserialize it
async fn get(stream: &mut TcpStream) -> Result<Protocol, Error> {
    match stream.readable().await {
        Ok(_) => (),
        Err(_) => return Err(Error::IOError),
    };

    let mut buf: [u8; MAX_READ_BYTES] = [0; MAX_READ_BYTES];
    match stream.read(&mut buf).await {
        Ok(_) => (),
        Err(_) => return Err(Error::IOError),
    };

    match bincode::deserialize(&buf) {
        Ok(m) => Ok(m),
        Err(_) => Err(Error::InvalidMessage),
    }
}

/// Get a Joined message
pub async fn get_join(stream: &mut TcpStream) -> Result<Protocol, Error> {
    match get(stream).await {
        Ok(m) => match m {
            Protocol::Join { .. } => Ok(m),
            _ => Err(Error::InvalidMessage),
        },
        Err(e) => Err(e),
    }
}

/// Get a Play message
pub async fn get_play(stream: &mut TcpStream) -> Result<Protocol, Error> {
    match get(stream).await {
        Ok(m) => match m {
            Protocol::Play {
                super_coords,
                mini_coords,
            } => {
                if super_coords.ok() && mini_coords.ok() {
                    Ok(m)
                } else {
                    Err(Error::InvalidMessage)
                }
            }
            _ => Err(Error::InvalidMessage),
        },
        Err(e) => Err(e),
    }
}

/// Handle forced end of game
pub async fn handle_end(stream: &mut TcpStream) -> Result<(), Error> {
    match end_game(stream).await {
        Ok(_) => Err(Error::OnePlayerDown),
        Err(_) => Err(Error::BothPlayersDown),
    }
}
